//
//  main.m
//  OPenCV3
//
//  Created by Admin on 10/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PIOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PIOAppDelegate class]));
   
    }
}
