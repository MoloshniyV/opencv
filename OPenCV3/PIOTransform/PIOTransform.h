//
//  PIOTransform.h
//  OPenCV3
//
//  Created by Admin on 10/25/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIOTransform : NSObject

- (cv::Mat)transformImageToCvMat:(UIImage *)image;
- (UIImage *)imageWithCVMat:(const cv::Mat&)cvMat;
- (UIImage *)UIImageFromIplImage:(IplImage *)image;
- (IplImage *)CreateIplImageFromUIImage:(UIImage *)image;

@end
