//
//  PIOViewController.m
//  OPenCV3
//
//  Created by Admin on 10/17/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "PIOViewController.h"
#import "PIOTransform.h"

#import <opencv2/highgui/cap_ios.h>
#import <opencv2/objdetect/objdetect.hpp>
#import <opencv2/imgproc/imgproc_c.h>


using namespace cv;

NSString* const faceCascadeFilename = @"haarcascade_frontalface_alt";

#ifdef __cplusplus
CascadeClassifier face_cascade;
#endif

//----------------------------------------------------------//---------------------------------------------------------------------//--------------------------------------------------------------//------------------------------------------
#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/ml/ml.hpp"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#endif
//----------------------------------------------------------//---------------------------------------------------------------------//--------------------------------------------------------------//------------------------------------------
@interface PIOViewController ()<CvVideoCameraDelegate>
{
}
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet UIImageView *image4;
@property (weak, nonatomic) IBOutlet UIImageView *image5;
@property (weak, nonatomic) IBOutlet UIImageView *image6;
@property (weak, nonatomic) IBOutlet UIImageView *image7;
@property (weak, nonatomic) IBOutlet UIImageView *image8;
@property (weak, nonatomic) IBOutlet UIImageView *image9;
@property (weak, nonatomic) IBOutlet UIImageView *image10;
@property (weak, nonatomic) IBOutlet UIImageView *image11;
@property (weak, nonatomic) IBOutlet UIImageView *image12;



@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic, retain) CvVideoCamera* videoCamera;
@property(assign, nonatomic)CascadeClassifier faceCascade;
@property(strong, nonatomic)PIOTransform *transform;

- (void)getData;
- (void)basicOCR;
- (void)sutupImageView:(NSInteger)index image:(IplImage *)image;
- (void)test;

@end


float classify(IplImage* img,int showResult);

char file_path[255];
int train_samples;
int classes;
CvMat* trainData;
CvMat* trainClasses;
int size;
static const int K=10;
CvKNearest *knn;


void train();

@implementation PIOViewController

#pragma mark GetData;

- (void) getData
{
    CvScalar s;
    IplImage* src_image;
    IplImage prs_image;
    CvMat row,data;
    char file[255];
    int i,j;
    for(i =0; i<classes; i++)
    {
        for( j = 0; j< train_samples; j++)
        {
            //Load file
            if(j<10)
                sprintf(file,"%s%d/%d0%d.pbm",file_path, i, i , j);
            else
                sprintf(file,"%s%d/%d%d.pbm",file_path, i, i , j);
            src_image = cvLoadImage(file,0);
            
//            for (int y =0; y<src_image->height; y++)
//                
//            {
//                for (int x =0; x<src_image->width; x++)
//                {
//                    s=cvGet2D(src_image,y,x);
//                    if (s.val[0] == 0)
//                    {
//                      //  printf("src X=%d , Y= %d Color=%f\n",x,y,s.val[0]);
//                    }
//                }
//            }
            
            if(!src_image)
            {
                printf("Error: Cant load image %s\n", file);
                //exit(-1);
            }
          
            prs_image = preprocessing(src_image, size, size);
            
            
            //Set class label
            
            cvGetRow(trainClasses, &row, i*train_samples + j);
            
            cvSet(&row, cvRealScalar(i));
            
            //Set data
            
            cvGetRow(trainData, &row, i*train_samples + j);
            
            
            
            IplImage* img = cvCreateImage( cvSize( size, size ), IPL_DEPTH_32F, 1 );
            
            //convert 8 bits image to 32 float image
            
            cvConvertScale(&prs_image, img, 0.0039215, 0);
            
            
            
            cvGetSubRect(img, &data, cvRect(0,0, size,size));
            
            
            
            CvMat row_header, *row1;
            
            //convert data matrix sizexsize to vecor
            
            row1 = cvReshape( &data, &row_header, 0, 1 );
            
            cvCopy(row1, &row, NULL);
            
            
        }
   }
 

  
    
}

#pragma mark Train;

void train()
{
    knn=new CvKNearest( trainData, trainClasses, 0, false, K );
}

float classify(IplImage* img, int showResult)

{
    CvScalar s;
   IplImage prs_image;
    
    CvMat data;
    
    CvMat* nearest=cvCreateMat(1,K,CV_32FC1);
    
    float result;
    
    //process file
    
    prs_image = *img;
//    
//    for (int y =0; y<prs_image.height; y++)
//    {
//        for (int x =0; x<prs_image.width; x++)
//        {
//            s=cvGet2D(&prs_image,y,x);
//            if  (s.val[0]==0)
//            {
//                 printf("X=%d , Y= %d Color=%f\n",x,y,s.val[0]);
//            }
//        }
//    }
 
    
    //Set data
    
    IplImage* img32 = cvCreateImage( cvSize( size, size ), IPL_DEPTH_32F, 1 );
    
    cvConvertScale(&prs_image, img32, 0.0039215, 0);
    
    cvGetSubRect(img32, &data, cvRect(0,0, size,size));
    
    CvMat row_header, *row1;
    
    row1 = cvReshape( &data, &row_header, 0, 1 );
    
    
    
    
   
    result=knn->find_nearest(row1,K,0,0,nearest,0);
    
    
    
    int accuracy=0;
    
    for(int i=0;i<K;i++){
        
        if( nearest->data.fl[i] == result)
            
            accuracy++;
        
    }
    
    
    
    float pre=100*((float)accuracy/(float)K);
    
    if(showResult==1){
        
        printf("|\t%.0f\t| \t%.2f%%  \t| \t%d of %d \t| \n",result,pre,accuracy,K);
        
        printf(" ---------------------------------------------------------------\n");
        
    }
    
    
    
    return result;
    
    
    
}

#pragma mark Test;

- (void) test
{
    IplImage* src_image;
    
    IplImage prs_image;
    
    CvMat row,data;
    
    char file[255];
    
    int i,j;
    
    int error=0;
    
    int testCount=0;
    CvScalar s;
    
    for(i =0; i<classes; i++){
        
        for( j = train_samples; j< train_samples+5; j++)
        {
            
            if(j<10)
                sprintf(file,"%s%d/%d0%d.pbm",file_path, i, i , j);
            else
                sprintf(file,"%s%d/%d%d.pbm",file_path, i, i , j);
            src_image = cvLoadImage(file,0);

            
            
            if(!src_image){
                
                printf("Error: Cant load image %s\n", file);
                
                //exit(-1);
                
            }
            
            //process file
            
            
            prs_image = preprocessing(src_image, size, size);
            
            float r=classify(&prs_image,0);
            
            if((int)r!=i)
                
                error++;
            
            
            
            testCount++;
            
        }
        
    }
    
    float totalerror=100*(float)error/(float)testCount;
    
    printf("System Error: %.2f%%\n", totalerror);
    
    
    
}

#pragma mark BASE;

- (void) basicOCR
{
    
    //initial
    sprintf(file_path , "/Volumes/work/OpenCvProject/pbm/");
    train_samples = 45;//50;
    classes = 2; //10;
    size=20;
    
    trainData = cvCreateMat(train_samples*classes, size*size, CV_32FC1);
    trainClasses = cvCreateMat(train_samples*classes, 1, CV_32FC1);
    
    //Get data (get images and process it)
    [self getData];
    //getData();
    
    //train
    train();
    //Test
    [self test];
   
//    IplImage* src_image;
//    IplImage prs_image;
//    
//    char file[255];
//   
//    sprintf(file,"/Volumes/work/OpenCvProject/jpg/010.jpg");
//    
//    src_image = cvLoadImage(file,0);
//    if(!src_image){
//        printf("Error: Cant load image %s\n", file);
//        //exit(-1);
//    }
//    //process file
//    prs_image = preprocessing(src_image, size, size);
//    
//    float r = classify(&prs_image,1);
   
    printf(" ---------------------------------------------------------------\n");
    printf("A|\tClass\t|\tPrecision\t|\tAccuracy\t|\n");
    printf(" ---------------------------------------------------------------\n");
    
    
}

void findX(IplImage* imgSrc,int *min, int *max)
{
    int i;
    
    int minFound=0;
    
    CvMat data;
    
    CvScalar maxVal = cvRealScalar((imgSrc->width * 255) );
    
    CvScalar val=cvRealScalar(0);
    
    //For each col sum, if sum < width*255 then we find the min
    
    //then continue to end to search the max, if sum< width*255 then is new max
    
    for (i=0; i< imgSrc->width; i++){
        
        cvGetCol(imgSrc, &data, i);
        
        val= cvSum(&data);
        
        if(val.val[0] < maxVal.val[0]){
            
            *max = i;
            
            if(!minFound){
                
                *min= i;
                
                minFound= 1;
                
            }
            
        }
        
    }
    
}

void findY(IplImage* imgSrc,int* min, int* max)
{
    int i;
    
    int minFound=0;
    
    CvMat data;
    
    CvScalar maxVal=cvRealScalar((imgSrc->width * 255) );
    
    CvScalar val=cvRealScalar(0);
    
    //For each col sum, if sum < width*255 then we find the min
    
    //then continue to end to search the max, if sum< width*255 then is new max
    
    for (i=0; i< imgSrc->height; i++){
        
        cvGetRow(imgSrc, &data, i);
        
        val= cvSum(&data);
        
        if(val.val[0] < maxVal.val[0]){
            
            *max=i;
            
            if(!minFound){
                
                *min= i;
                
                minFound= 1;
                
            }
            
        }
        
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self setTransform:[[PIOTransform alloc] init]];
    
    [self basicOCR];
}

#pragma mark find BB;

CvRect findBB(IplImage* imgSrc)
{
    
    CvRect aux;
    
    int xmin, xmax, ymin, ymax;
    
    xmin=xmax=ymin=ymax=0;
    
    
    
    findX(imgSrc, &xmin, &xmax);
    
    findY(imgSrc, &ymin, &ymax);
    
    
    aux = cvRect(xmin, ymin, xmax-xmin+1, ymax-ymin+1);
    

    //printf("BB: %d,%d - %d,%d\n", aux.x, aux.y, aux.width, aux.height);
    
    
    return aux;
}

#pragma mark PROCESSING;

IplImage preprocessing(IplImage* imgSrc,int new_width, int new_height)
{
    
    IplImage* result;
    
    IplImage* scaledResult;
    
    
    
    CvMat data;
    
    CvMat dataA;
    
    CvRect bb;//bounding box
    
    CvRect bba;//boundinb box maintain aspect ratio
    
    
    
    //Find bounding box
    
    bb=findBB(imgSrc);
    
    
    
    //Get bounding box data and no with aspect ratio, the x and y can be corrupted
    
    cvGetSubRect(imgSrc, &data, cvRect(bb.x, bb.y, bb.width, bb.height));
    
    //Create image with this data with width and height with aspect ratio 1
    
    //then we get highest size betwen width and height of our bounding box
    
    int size=(bb.width>bb.height)?bb.width:bb.height;
    
    result=cvCreateImage( cvSize( size, size ), 8, 1 );
    
    cvSet(result,CV_RGB(255,255,255),NULL);
    
    //Copy de data in center of image

    int x=(int)floor((float)(size-bb.width)/2.0f);
    
    int y=(int)floor((float)(size-bb.height)/2.0f);
    
    cvGetSubRect(result, &dataA, cvRect(x,y,bb.width, bb.height));
    
    cvCopy(&data, &dataA, NULL);
    
    //Scale result
    
    scaledResult=cvCreateImage( cvSize( new_width, new_height ), 8, 1 );
    
    cvResize(result, scaledResult, CV_INTER_NN);
    
    
    
    //Return processed data
    
    return *scaledResult;
    
    
    
}

//void getData()
//{
//    IplImage* src_image;
//    
//    IplImage prs_image;
//    
//    CvMat row,data;
//    
//    char file[255];
//    
//    int i,j;
//    
//    for(i =0; i<classes; i++){
//        
//        for( j = 0; j< train_samples; j++){
//            
//            
//            
//            //Load file
//            
//            if(j<10)
//                
//                sprintf(file,"%s%d/%d0%d.pbm",file_path, i, i , j);
//            
//            else
//                
//                sprintf(file,"%s%d/%d%d.pbm",file_path, i, i , j);
//            
//            src_image = cvLoadImage(file,0);
//            
//            if(!src_image){
//                
//                printf("Error: Cant load image %s\n", file);
//                
//                //exit(-1);
//                
//            }
//            
//            //process file
//            
//            prs_image = preprocessing(src_image, size, size);
//            
//            
//            
//            //Set class label
//            
//            cvGetRow(trainClasses, &row, i*train_samples + j);
//            
//            cvSet(&row, cvRealScalar(i));
//            
//            //Set data
//            
//            cvGetRow(trainData, &row, i*train_samples + j);
//            
//            
//            
//            IplImage* img = cvCreateImage( cvSize( size, size ), IPL_DEPTH_32F, 1 );
//            
//            //convert 8 bits image to 32 float image
//            
//            cvConvertScale(&prs_image, img, 0.0039215, 0);
//            
//            
//            
//            cvGetSubRect(img, &data, cvRect(0,0, size,size));
//            
//            
//            
//            CvMat row_header, *row1;
//            
//            //convert data matrix sizexsize to vecor
//            
//            row1 = cvReshape( &data, &row_header, 0, 1 );
//            
//            cvCopy(row1, &row, NULL);
//            
//        }
//        
//    }
//    
//}
//





- (IBAction)actionStart:(id)sender
{
  //  [self.videoCamera start];
}

- (void)processImage:(Mat&)image
{
    vector<cv::Rect> faces;
    Mat frame_gray;
    
    cvtColor(image, frame_gray, CV_BGRA2GRAY);
    equalizeHist(frame_gray, frame_gray);
    
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(100, 100));
    
    for(unsigned int i = 0; i < faces.size(); ++i) {
        rectangle(image, cv::Point(faces[i].x, faces[i].y),
                  cv::Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height),
                  cv::Scalar(0,255,255));
    }
}

- (void)sutupImageView:(NSInteger)index image:(IplImage *)image
{
    switch (index)
    {
        case 0:
            [_image1 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 1:
            [_image2 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 2:
            [_image3 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 3:
            [_image4 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 4:
            [_image5 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 5:
            [_image6 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 6:
            [_image7 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 7:
            [_image8 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 8:
            [_image9 setImage:[_transform UIImageFromIplImage:image]];
            break;
        case 9:
            [_image10 setImage:[_transform UIImageFromIplImage:image]];
            break;
            
        default:
            break;
    }

}

@end
